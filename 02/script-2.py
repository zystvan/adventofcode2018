#!/usr/bin/python3

input_file = open('input.txt', 'r')
lines = []

for line in input_file.readlines():
  line = line.replace("\n", "")
  
  lines.append(line)

for line in lines:
  for line2 in lines:
    if line2 == line:
      continue

    chars = list(line)
    common_chars = []

    for i in range(len(line2)):
      if line2[i] == chars[i]:
        common_chars.append(line2[i])

    if len(common_chars) == len(chars) - 1:
      print("".join(common_chars))
      exit()