#!/usr/bin/python3

input_file = open('input.txt', 'r')
num_two = 0
num_three = 0

for line in input_file.readlines():
  line = line.replace("\n", "")
  chars = {}

  for char in line:
    if char in chars:
      chars[char] += 1
    else:
      chars[char] = 1

  if 2 in chars.values():
    num_two += 1
  
  if 3 in chars.values():
    num_three += 1

print(num_two * num_three)