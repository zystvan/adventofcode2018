#!/usr/bin/python3

input_file = open('input.txt', 'r')
file_contents = input_file.readlines()
freqs = []
current_freq = 0
current_line = 0

while True:
  line = file_contents[current_line % len(file_contents)]
  val = int(line)

  current_freq += val

  if current_freq in freqs:
    print(current_freq)
    exit()
  else:
    freqs.append(current_freq)
    current_line += 1

input_file.close()