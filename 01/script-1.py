#!/usr/bin/python3

input_file = open('input.txt', 'r')
input_vals = []

for val in input_file.readlines():
  input_vals.append(int(val))

print(sum(input_vals))
input_file.close()