#!/usr/bin/python3

input_file = open('input.txt', 'r')
lines = []
claimed_squares = {}
overlapping_squares = {}

# process the input
for line in input_file.readlines():
  line = line.strip("\n").split(" ")
  coords = [int(x) for x in line[2].strip(":").split(",")]

  lines.append({\
    "id": line[0][1:], \
    "area_x": range(coords[0], coords[0] + int(line[3].split("x")[0])), \
    "area_y": range(coords[1], coords[1] + int(line[3].split("x")[1])), \
  })

# find overlapping squares
for line in lines:
  for x in line["area_x"]:
    if x not in claimed_squares:
      claimed_squares[x] = []

    for y in line["area_y"]:
      if y not in claimed_squares[x]:
        claimed_squares[x].append(y)
      else:
        if x not in overlapping_squares:
          overlapping_squares[x] = [y]
        else:
          overlapping_squares[x].append(y)

# print result
deduped_squares = 0

for i in overlapping_squares:
  deduped_squares += len(set(overlapping_squares[i]))

print(deduped_squares)